typedef struct obj_attrs {
	u16 attr0;
	u16 attr1;
	u16 attr2;
	u16 fill;
} __attribute__((packed, aligned(4))) obj_attr;

/*
attr0:				attr1:				attr2:
ATTR0_Y#			ATTR1_X#			ATTR2_ID#
ATTR0_MODE#			ATTR1_AFF#			ATTR2_PRIO#
ATTR0_GFX#			ATTR1_FLIP#			ATTR2_PALBANK#
ATTR0_MOSAIC		ATTR1_SIZE#
color mode
ATTR0_SHAPE#

*/


/*
########## ATTR0 #######
*/
#define ATTR0_Y_MASK 0x00FF
#define ATTR0_Y_SHIFT 0

#define ATTR0_MODE_MASK 0x0300
#define ATTR0_MODE_SHIFT 8	

#define ATTR0_GFX_MASK 0x0300
#define ATTR0_GFX_SHIFT 10

#define ATTR0_MOSAIC_MASK 0xC000
#define ATTR0_MOSAIC_SHIFT 12

#define ATTR0_COLOR_MASK 0x1000
#define ATTR0_COLOR_SHIFT 13

#define ATTR0_SHAPE_MASK 0xC000
#define ATTR0_SHAPE_SHIFT 14


inline obj_attr* setY(obj_attr* obj, u16 y){
	obj->attr0 = (obj->attr0 & ~ATTR0_Y_MASK) |	 ( (y << ATTR0_Y_SHIFT) & ATTR0_Y_MASK);
	return obj;	
}

inline obj_attr* setObjectMode(obj_attr* obj, u16 mode){ /* ATTR0_REG, ATTR0_AFF, ATTR0_HIDE, ATTR0_AFF_DBL */
	obj->attr0 = (obj->attr0 & ~ATTR0_MODE_MASK) |	 (( mode << ATTR0_MODE_SHIFT) & ATTR0_MODE_MASK);
	return obj;	
}

inline obj_attr* setGfxMode(obj_attr* obj, u16 mode){
	obj->attr0 = (obj->attr0 & ~ATTR0_GFX_MASK) |	 (( mode << ATTR0_GFX_SHIFT) & ATTR0_GFX_MASK);
	return obj;
}

inline obj_attr* setMosaic(obj_attr* obj, u16 mosaic){
	obj->attr0 = (obj->attr0 & ~ATTR0_MOSAIC_MASK) |	 (( mosaic << ATTR0_MOSAIC_SHIFT) & ATTR0_MOSAIC_MASK);
	return obj;	
}

inline obj_attr* setColorMode(obj_attr* obj, u16 mode){
	obj->attr0 = (obj->attr0 & ~ATTR0_COLOR_MASK) |	 (( mode << ATTR0_COLOR_SHIFT) & ATTR0_COLOR_MASK);
	return obj;	
}

inline obj_attr* setSpriteShape(obj_attr* obj, u16 mode){
	obj->attr0 = (obj->attr0 & ~ATTR0_SHAPE_MASK) |	 (( mode << ATTR0_SHAPE_SHIFT) & ATTR0_SHAPE_MASK);
	return obj;	
}

/*
######## ATTR1 #######
*/
#define ATTR1_X_MASK 0x01FF
#define ATTR1_X_SHIFT 0

#define ATTR1_AFF_MASK 0x3E00
#define ATTR1_AFF_SHIFT 9

#define ATTR1_VFLIP_MASK 0x2000
#define ATTR1_VFLIP_SHIFT 13

#define ATTR1_HFLIP_MASK 0x1000
#define ATTR1_HFLIP_SHIFT 12

#define ATTR1_SIZE_MASK 0xC000
#define ATTR1_SIZE_SHIFT 14

inline obj_attr* setX(obj_attr* obj, u16 x){
	obj->attr1 = (obj->attr1 & ~ATTR1_X_MASK) |	 (( x << ATTR1_X_SHIFT) & ATTR1_X_MASK);
	return obj;	
}

inline obj_attr* setAffineIndex(obj_attr* obj, u16 index){
	obj->attr1 = (obj->attr1 & ~ATTR1_AFF_MASK) |	 (( index << ATTR1_AFF_SHIFT) & ATTR1_AFF_MASK);
	return obj;	
}

inline obj_attr* setSpriteVerticalFlip(obj_attr* obj, u16 flip){
	obj->attr1 = (obj->attr1 & ~ATTR1_VFLIP_MASK) |	 (( flip << ATTR1_HFLIP_SHIFT) & ATTR1_HFLIP_MASK);
	return obj;	
}

inline obj_attr* setSpriteHorizontalFlip(obj_attr* obj, u16 flip){
	obj->attr1 = (obj->attr1 & ~ATTR1_HFLIP_MASK) |	 (( flip << ATTR1_HFLIP_SHIFT) & ATTR1_HFLIP_MASK);
	return obj;	
}

inline obj_attr* setSpiteSize(obj_attr* obj, u16 size){
	obj->attr1 = (obj->attr1 & ~ATTR1_SIZE_MASK) |	 (( size << ATTR1_SIZE_SHIFT) & ATTR1_SIZE_MASK);
	return obj;	
}


/*
####### ATTR2 ######
*/

#define ATTR2_ID_MASK 0x02FF
#define ATTR2_ID_SHIFT 0

#define ATTR2_PRIO_MASK 0x0B00
#define ATTR2_PRIO_SHIFT 10

#define ATTR2_PALBANK_MASK 0xF000
#define ATTR2_PALBANK_SHIFT 14


inline obj_attr* setTileIndex(obj_attr* obj, u16 tileIndex){
	obj->attr2 = (obj->attr2 & ~ATTR2_ID_MASK) |	 (( tileIndex << ATTR2_ID_SHIFT) & ATTR2_ID_MASK);
	return obj;	
}

inline obj_attr* setPrio(obj_attr* obj, u16 tileIndex){
	obj->attr2 = (obj->attr2 & ~ATTR2_PRIO_MASK) |	 (( tileIndex << ATTR2_PRIO_SHIFT) & ATTR2_PRIO_MASK);
	return obj;	
}


inline obj_attr* setPaletteBank(obj_attr* obj, u16 tileIndex){
	obj->attr2 = (obj->attr2 & ~ATTR2_PALBANK_MASK) |	 (( tileIndex << ATTR2_PALBANK_SHIFT) & ATTR2_PALBANK_MASK);
	return obj;
}