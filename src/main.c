#define SCREEN_WIDTH  240
#define SCREEN_HEIGHT 160

#define MEM_IO   0x04000000
#define MEM_VRAM 0x06000000

#define MEM_OAM  0x07000000 
//Object attributes memory
//Until 0x0700:0400

#define oam_mem            ((volatile obj_attrs *)MEM_OAM)
#define REG_DISPLAY        (*((volatile u32 *)(MEM_IO)))
#define REG_DISPLAY_VCOUNT (*((volatile u32 *)(MEM_IO + 0x0006)))


typedef unsigned char  u8;
typedef unsigned short u16;
typedef unsigned int   u32;

#include <controller.h>
#include <palette.h>
#include <objects.h>

int main(void)
{
	
	REG_DISPLAY = 0x1000 | 0x0040;

	// The main game loop
	while (1) {
		// vsync
		while(REG_DISPLAY_VCOUNT >= 160);
		while(REG_DISPLAY_VCOUNT <  160);

		//update();


	}

	return 0;
}