DEVKIT = /opt/devkitpro/devkitARM/bin/arm-none-eabi-gcc
DEVKIT_GBA = /opt/devkitpro/devkitARM/bin/arm-none-eabi-objcopy
ROMNAME = main

FLAGS = -mthumb-interwork -mthumb
INC = includes/
OUT = out/
OO = $(OUT)objects/


all : main.o
	$(DEVKIT) $(OO)/main.o $(FLAGS) -specs=gba.specs -o $(OUT)main.elf
	$(DEVKIT_GBA) -v -O binary out/main.elf $(OUT)$(ROMNAME).gba
	gbafix $(OUT)$(ROMNAME).gba
main.o :
	$(DEVKIT) -I $(INC) -c src/main.c $(FLAGS) -O2 -o $(OO)/main.o

launch :
	vbam $(OUT)$(ROMNAME).gba
clean :
	rm out/*.elf
	rm out/*.gba
	rm $(OO)*.o
